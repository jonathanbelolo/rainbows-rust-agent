use std::sync::Arc;
use tokio::sync::Mutex;

use core::agent::store;
use core::agent::command::Command::*;
use core::did::peer::Identity;

use super::{client, server};

pub type Store = Arc<Mutex<store::Store>>;

#[derive(Debug)]
pub struct Runtime {
    pub store: Store,
    pub endpoint: String
}

impl Runtime {

    pub fn new<T: AsRef<str>>(mut root: Identity, endpoint: T) -> Self {
        let endpoint = endpoint.as_ref().to_owned();
        root.root_doc.add_endpoint(endpoint.clone());
        Runtime {
            store: Arc::new(Mutex::new(store::Store::new(root))),
            endpoint
        }
    }

    pub async fn start(&self) {
        server::start(self.store.clone(), self.endpoint.parse().unwrap()).await
    }

    pub async fn execute(&mut self, action: store::Action) {
        let store = self.store.clone();
        Runtime::run_execute(store, action).await;
    }

    pub async fn notify(&mut self, event: store::Event) {
        let store = self.store.clone();
        Runtime::run_notify(store, event).await;
    }

    pub async fn run_execute(store: Store, action: store::Action) {
        let mut action_stack = vec![action];
        while let Some(action) = action_stack.pop() {
            let commands = vec![store.lock().await.execute(action.clone())];
            let cmd_stack = Arc::new(Mutex::new(commands));
            while let Some(cmd) = cmd_stack.lock().await.pop() {
                match cmd {
                    Nothing => break,
                    HttpGet(url, next) => {
                        println!("GET {}", &url);
                        let response = client::get(url).await;
                        action_stack.push(next(response));
                    },
                    SendMessage(message, url, handle) => {
                        println!("Execute Send Message {}", &url);
                        let store = store.clone();
                        let cmd_stack = cmd_stack.clone();
                        tokio::spawn(async move {
                            client::send_message(message, url).await;
                            let cmd = store.lock().await.notify(store::Event::MessageSent(handle));
                            cmd_stack.lock().await.push(cmd);
                        });
                    }
                }
            }
        }
    }

    pub async fn run_notify(store: Store, event: store::Event) {
        let commands = vec![store.lock().await.notify(event)];
        let stack = Arc::new(Mutex::new(commands));
        while let Some(cmd) = stack.lock().await.pop() {
            match cmd {
                SendMessage(message, url, handle) => {
                    println!("Notify Send Message {}", &url);
                    let store = store.clone();
                    let stack = stack.clone();
                    tokio::spawn(async move {
                        client::send_message(message, url).await;
                        let next_command = store.lock().await.notify(store::Event::MessageSent(handle));
                        stack.lock().await.push(next_command);
                    });
                },
                _ => {}
            }
        }
    }

} 

