use agent;
use core::did::peer::Identity;
use core::agent::store::Action::ConnectWith;
use agent::agent::runtime::Runtime;

#[tokio::main]
async fn main() {
    agent::init();
    let nanou = Identity::new("Nanou".to_owned());
    let mut runtime = Runtime::new(nanou, "127.0.0.1:3040");
    runtime.execute(ConnectWith("127.0.0.1:3030".into())).await;
    runtime.start().await;
}

