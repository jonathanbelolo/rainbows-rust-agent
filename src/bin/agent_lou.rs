use agent;
use core::did::peer::Identity;
use agent::agent::runtime::Runtime;

#[tokio::main]
async fn main() {
    agent::init();
    let lou = Identity::new("Lou Rose".to_owned());
    let runtime = Runtime::new(lou, "127.0.0.1:3030");
    runtime.start().await;
}