## RAINBOWS RUST AGENT

A pure Rust implementation of the following:

https://identity.foundation/didcomm-messaging/docs/spec/#did-exchange-protocol

https://identity.foundation/peer-did-method-spec/index.html

## Usage

To run the demo in your terminal:

`cargo run --bin=agent_lou`

Then in a separate terminal:

`cargo run --bin=agent_nanou`

Both agents should connect, follow the protocol, and end up in status "Completed"

© Lightways SAS 2020

