use crate::agent::{
    runtime::{Store, Runtime},
};

use core::agent::store::Event::*;


use std::collections::HashMap;

use warp::Filter;
use warp::http::StatusCode;

use tokio;

use std::convert::Infallible;

pub async fn start(store: Store, addr: std::net::SocketAddrV4) {

    let serve_invite = warp::path!("invitation")
        .and(warp::get())
        .and(with_store(store.clone()))
        .and_then(get_invitation);

    let handle_msg = warp::path!("didcomm")
        .and(warp::post())
        .and(warp::body::form())
        .and(with_store(store.clone()))
        .and_then(handle_message);

    let routes = serve_invite.or(handle_msg);

    println!("\nServer listening on: {}\n", addr);

    warp::serve(routes)
        .run(addr)
        .await;
}

fn with_store(store: Store) -> impl Filter<Extract = (Store,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || store.clone())
}

async fn handle_message(msg: HashMap<String, String>, store: Store) -> Result<impl warp::Reply, Infallible> {
    tokio::spawn(async move {
        let packed = msg.get("message".into()).unwrap();
        Runtime::run_notify(store, MessageReceived(packed.to_owned())).await;
    });
    Ok(StatusCode::CREATED)
}

async fn get_invitation(store: Store) -> Result<impl warp::Reply, Infallible> {
    let st = store.clone();
    let mut store = store.lock().await;
    let (invitation, handle) = store.create_invitation();
    println!("{}", invitation);
    tokio::spawn(async move {
        Runtime::run_notify(st, MessageSent(handle)).await;
    });
    Ok(warp::reply::html(invitation))
}