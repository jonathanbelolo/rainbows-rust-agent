
pub async fn get(peer: String) -> String {
    reqwest::get(&format!("http://{}", peer))
    .await.unwrap()
    .text()
    .await.unwrap()
}

pub async fn send_message(message: String, peer: String) {
    println!("\nSending Message: {}", message);
    let client = reqwest::Client::new();
    let res = client.post(&format!("http://{}/didcomm", peer))
        .form(&[("message", message)])
        .send()
        .await.unwrap();
    println!("{:#?}", res);
}